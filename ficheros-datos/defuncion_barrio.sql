CREATE TABLE IF NOT EXISTS `defuncion_barrio` (
  `id`  int(20) AUTO_INCREMENT PRIMARY KEY,
  `total` int(15) DEFAULT NULL,
  `homes` int(15) DEFAULT NULL,
  `dones` int(15) DEFAULT NULL,
  `barrio` varchar(250) DEFAULT NULL,
  `anyo` int(4) DEFAULT NULL
);