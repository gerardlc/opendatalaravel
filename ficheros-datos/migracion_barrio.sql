-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 11-05-2016 a las 20:32:09
-- Versión del servidor: 5.6.24
-- Versión de PHP: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `opendatacollector`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migracion_barrio`
--

CREATE TABLE IF NOT EXISTS `migracion_barrio` (
  `id` bigint(20) NOT NULL,
  `numero_personas` int(11) NOT NULL,
  `anyo` int(11) DEFAULT NULL,
  `barrio_origen_id` bigint(20) DEFAULT NULL,
  `barrio_destino_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `migracion_barrio`
--
ALTER TABLE `migracion_barrio`
  ADD PRIMARY KEY (`id`), ADD KEY `fk_migracionbarrio_barrioorigen_id` (`barrio_origen_id`), ADD KEY `fk_migracionbarrio_barriodestino_id` (`barrio_destino_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `migracion_barrio`
--
ALTER TABLE `migracion_barrio`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `migracion_barrio`
--
ALTER TABLE `migracion_barrio`
ADD CONSTRAINT `fk_migracionbarrio_barriodestino_id` FOREIGN KEY (`barrio_destino_id`) REFERENCES `barrio` (`id`),
ADD CONSTRAINT `fk_migracionbarrio_barrioorigen_id` FOREIGN KEY (`barrio_origen_id`) REFERENCES `barrio` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
