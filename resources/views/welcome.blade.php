@extends('layouts.app')

@section('content')

    <div id="index-banner" class="parallax-container">
        <div class="section no-pad-bot">
            <div class="container">
                <br><br>
                <h1 class="header center teal-text text-lighten-2">Open Data Collector</h1>
                <div class="row center">
                    <h5 class="header col s12 light">Un portal moderno de open data en español</h5>
                </div>
                <div class="row center">
                    <a href="{{route('dataset.index')}}" id="download-button" class="btn-large waves-effect waves-light teal lighten-1">Ver catálogo de datos</a>
                </div>
                <br><br>

            </div>
        </div>
        <div class="parallax"><img src="{{asset('img/open3.jpg')}}" alt="Unsplashed background img 1"></div>
    </div>


    <div class="container">
        <div class="section">

            <!--   Icon Section   -->
            <div class="row">
                <div class="col s12 m4">
                    <div class="icon-block">
                        <h2 class="center brown-text"><i class="medium material-icons">view_module</i></h2>
                        <h5 class="center">Tablas</h5>

                        <p class="light">Te ofrecemos los datos en formato tabla para que los consultes desde donde quieras. Lo mejor para echar un vistazo sin usar programas externos.</p>
                    </div>
                </div>

                <div class="col s12 m4">
                    <div class="icon-block">
                        <h2 class="center brown-text"><i class="medium material-icons">trending_up</i></h2>
                        <h5 class="center">Gráficas</h5>

                        <p class="light">Te mostramos gráficas que interpretan los datos para que de un simple vistazo sepas interpretar los datos que estas consultando.</p>
                    </div>
                </div>

                <div class="col s12 m4">
                    <div class="icon-block">
                        <h2 class="center brown-text"><i class="medium material-icons">play_for_work</i></h2>
                        <h5 class="center">Descargas</h5>

                        <p class="light">Puedes descargar los archivos con los datos. Y si tienes alguno mas actualizado no dudes en hacernoslo saber.</p>
                    </div>
                </div>
            </div>

        </div>
    </div>


    <div class="parallax-container valign-wrapper">
        <div class="section no-pad-bot">
            <div class="container">
                <div class="row center">
                    <h5 class="header col s12">Consulta datos de una forma sencilla y moderna.</h5>
                </div>
            </div>
        </div>
        <div class="parallax"><img src="{{asset('img/open2.jpg')}}" alt="Unsplashed background img 2"></div>
    </div>

    <div class="container">
        <div class="section">

            <div class="row">
                <div class="col s12 center">
                    <h3><i class="mdi-content-send brown-text"></i></h3>
                    <h4>Contacto</h4>
                    <p class="left-align light">No dudes en ponerte en contacto con nosotros para cualquier cosa.
                        Aceptamos todo tipo de críticas, mejoras y sugerencias. Si crees que nos faltan datos y tu los tienes envianoslos y los revisaremos para añadirlos.
                     Si algun dato es incorrecto haznos saberlo, si el diseño no es de tu agrado comentanoslo, si te interesa el proyecto profesionalmente envíanos un email y si lo único que quieres decirnos es "hola" aquí estaremos.</p>
                </div>
            </div>

        </div>
    </div>


    <div class="parallax-container valign-wrapper">
        <div class="container">

                <div class="row center">
                    <h5 class="header col s12 ">Un diseño que te enganchará como nunca antes lo había hecho otro sitio de open data.</h5>
                </div>

        </div>
        <div class="parallax"><img src="{{asset('img/open1.jpg')}}" alt="Unsplashed background img 3"></div>
    </div>

    <footer class="page-footer teal">
        <div class="container">
            <div class="row">
                <div class="col l6 s12">
                    <h5 class="white-text">Información</h5>
                    <p class="grey-text text-lighten-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dicta earum est nam provident. Ad adipisci debitis quaerat quis veniam. Autem consequatur debitis eveniet, fuga non perferendis soluta temporibus. Dolore, ut.</p>


                </div>
                <div class="col l3 s12">
                    <h5 class="white-text">Settings</h5>
                    <ul>
                        <li><a class="white-text" href="#!">Link 1</a></li>
                        <li><a class="white-text" href="#!">Link 2</a></li>
                        <li><a class="white-text" href="#!">Link 3</a></li>
                        <li><a class="white-text" href="#!">Link 4</a></li>
                    </ul>
                </div>
                <div class="col l3 s12">
                    <h5 class="white-text">Connect</h5>
                    <ul>
                        <li><a class="white-text" href="#!">Link 1</a></li>
                        <li><a class="white-text" href="#!">Link 2</a></li>
                        <li><a class="white-text" href="#!">Link 3</a></li>
                        <li><a class="white-text" href="#!">Link 4</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="footer-copyright">
            <div class="container">
                by <a class="brown-text text-lighten-3" href="">Gerardlc</a>
            </div>
        </div>
    </footer>
@endsection
