@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col s12 titulo1 z-depth-1">
            <div class="container">
                <h3 class="center white-text">Listado de subcategorias</h3>
            </div>
        </div>
    </div>
    <div class="container">

        <div class="row">
            @foreach($subcategories as $subcategory)

                <div class="col s12 m6">
                    <div class="card blue darken-3 z-depth-2">
                        <div class="card-content white-text">
                            <span class="card-title"><a href="{{url('dataset/subcat',$subcategory->id)}}">{{$subcategory->name}}</a></span>
                            <p>{{$subcategory->information}}</p>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>

    </div>
@endsection