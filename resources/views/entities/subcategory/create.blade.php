@extends('layouts.app')
@section('content')
<div class="container">
    <h1>Crear subcategoria</h1>

    <form action="{{ route('subcategory.store')}}" method="post">
        <div class="form-group">
            <label for="nombre">Nombre</label>
            <input class="form-control" type="text" name="nombre" id="nombre">
        </div>

        <div class="form-group">
            <label for="informacion">Informacion</label>
            <input class="form-control" type="text" name="informacion" id="informacion">
        </div>
        <div class="form-group">
            <label for="imagen">Imagen</label>
            <input class="form-control" type="text" name="imagen" id="imagen" value="/uploads">
        </div>

        <div class="form-group">
            <label for="categoria">Categoria</label>
            <input class="form-control" type="text" name="categoria" id="categoria">
        </div>

        <button class="btn btn-default" type="submit">Enviar</button>
        <input type="hidden" value="{{Session::token()}}" name="_token">
    </form>
</div>
@endsection