@extends('layouts.app')
@section('content')
<div class="container">
    <h1>Crear dataset</h1>

    <form action="{{ route('dataset.store')}}" method="post">
        <div class="form-group">
            <label for="nombre">Nombre</label>
            <input class="form-control" type="text" name="nombre" id="nombre">
        </div>

        <div class="form-group">
            <label for="informacion">Informacion</label>
            <input class="form-control" type="text" name="informacion" id="informacion">
        </div>
        <div class="form-group">
            <label for="tabla">Tabla</label>
            <input class="form-control" type="text" name="tabla" id="tabla">
        </div>

        <div class="form-group">
            <label for="descargas">Descargas</label>
            <input class="form-control" type="text" name="descargas" id="descargas">
        </div>

        <div class="form-group">
            <label for="categoria">Categoria</label>
            <input class="form-control" type="text" name="categoria" id="categoria">
        </div>

        <div class="form-group">
            <label for="subcategoria">Subcategoria</label>
            <input class="form-control" type="text" name="subcategoria" id="subcategoria">
        </div>

        <button class="btn btn-default" type="submit">Enviar</button>
        <input type="hidden" value="{{Session::token()}}" name="_token">

    </form>
</div>
@endsection