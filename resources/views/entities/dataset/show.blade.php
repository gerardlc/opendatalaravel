@extends('layouts.app')
@section('content')

    <script type="text/javascript">
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        // Load the Visualization API and the corechart package.
        google.charts.load('current', {'packages': ['corechart']});

        function pintarGrafica() {

            var label = $('input[name=group1]:checked', '#label').val();
            var value = $('input[name=group2]:checked', '#value').val();

            var chart = $('select#chartType').val();





            if(chart == 'pie'){
                google.charts.setOnLoadCallback(drawChart(label, value));
            }else if(chart == 'bar'){
                google.charts.setOnLoadCallback(drawBarChart(label, value));
            }
            // Set a callback to run when the Google Visualization API is loaded.
            //google.charts.setOnLoadCallback(drawChart(label, value));

            function drawChart(label, value) {
                $("#chart_div").css("border","6px solid #26A69A");

                var datos2 = $.ajax({
                    url: "datos2",
                    type: 'post',
                    data: {_token: CSRF_TOKEN, 'label': label, 'value': value},
                    dataType: 'JSON',
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader('X-CSRF-TOKEN', $("#token").attr('content'));
                    },
                    async: false

                }).done(function (results) {

                    // Create the data table.
                    var data = new google.visualization.DataTable();
                    data.addColumn('string', 'Topping');
                    data.addColumn('number', 'Slices');

                    for (var i in results) {
                        data.addRow([results[i][label], results[i][value]]);
                    }
                    // Instantiate and draw our chart, passing in some options.
                    var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
                    // Set chart options
                    var options = {
                        'title': 'RESULTADO',
                        'width': 800,
                        'height': 800,
                        is3D: true,
                        legend: {position: 'right'},
                        pieSliceText: 'none',
                        //pieHole: 0.4,
                    };
                    chart.draw(data, options);
                });
            }
            function drawBarChart() {


                var datos2 = $.ajax({
                    url: "datos2",
                    type: 'post',
                    data: {_token: CSRF_TOKEN, 'label': label, 'value': value},
                    dataType: 'JSON',
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader('X-CSRF-TOKEN', $("#token").attr('content'));
                    },
                    async: false

                }).done(function (results) {

                    // Create the data table.
                    var data = new google.visualization.DataTable();
                    data.addColumn('string', 'Topping');
                    data.addColumn('number', 'Slices');

                    for (var i in results) {
                        data.addRow([results[i][label], results[i][value]]);
                    }
                    // Instantiate and draw our chart, passing in some options.
                    var chart = new google.visualization.BarChart(document.getElementById('chart_div'));
                    // Set chart options


                    // set inner height to 30 pixels per row
                    var chartAreaHeight = data.getNumberOfRows() * 30;
                    // add padding to outer height to accomodate title, axis labels, etc
                    var chartHeight = chartAreaHeight + 80;
                    var chart = new google.visualization.BarChart(document.querySelector('#chart_div'));
                    var options = {
                        title: 'cabecera',
                        width: 1000,
                        height: chartHeight,
                        chartArea: {
                            height: chartAreaHeight
                        },
                        hAxis: {
                            title: 'titulo haxis',
                            minValue: 0
                        },
                        vAxis: {
                            title: 'City'
                        }
                    };
                    chart.draw(data, options);
                });
            }
        }

    </script>


    <div class="row">
        <div class="col s12 titulo1 z-depth-1">
            <div class="container">
                <h3 class="center white-text">Listado de datasets</h3>


            </div>
        </div>
    </div>
    <div class="container">

        <div class="row">
            <div class="col s12">
                <ul class="tabs">
                    <li class="tab col s3"><a href="#test1">Tabla</a></li>
                    <li class="tab col s3"><a href="#test2">Gráficas</a></li>
                    <li class="tab col s3"><a href="#test3">Descargas</a></li>
                </ul>
            </div>
            {{--<a href="#" id="get">Get</a>--}}
            <div id="test1" class="col s12">

                <table class="highlight">
                    <thead>
                    <tr>
                        @for ($i = 1; $i < count($columns); $i++)
                            <th>{{ $columns[$i] }}</th>
                        @endfor

                    </tr>
                    </thead>

                    <tbody>


                    @foreach($rows as $row)
                        <tr>
                            @for ($i = 1; $i < count($columns); $i++)
                                <td>{{ $row->$columns[$i] }}</td>
                        @endfor
                        <tr>
                    @endforeach

                    </tbody>
                </table>

                <div class="row">
                    <div class="col s12 center">
                        <?php echo $rows->links(); ?>
                    </div>
                </div>
            </div>

            <div id="test2" class="col s12">

                <div class="row">
                    <div class="input-field col s12">

                        <select id="chartType">
                            <option value="" disabled selected>Escoje gráfica</option>
                            <option value="pie">Pie</option>
                            <option value="bar">Bar</option>
                            <option value="bubble">Bubble</option>
                        </select>
                        <label>Escoje gráfica</label>
                    </div>

                    <div class="input-field col s6">

                        <h4>Escoje la etiqueta</h4>
                        <form id="label" action="#">
                            @for ($i = 1; $i < count($columns); $i++)
                                <p>
                                    <input name="group1" type="radio" id="{{ $columns[$i] }}-label"
                                           value="{{ $columns[$i] }}"/>
                                    <label for="{{ $columns[$i] }}-label">{{ $columns[$i] }}</label>
                                </p>
                            @endfor
                        </form>

                    </div>

                    <div class="input-field col s6">

                        <h4>Escoje los valores</h4>
                        <form id="value" action="#">
                            @for ($i = 1; $i < count($columns); $i++)

                                <p>
                                    <input name="group2" type="radio" id="{{ $columns[$i] }}-value"
                                           value="{{ $columns[$i] }}" disabled/>
                                    <label for="{{ $columns[$i] }}-value">{{ $columns[$i] }}</label>
                                </p>
                            @endfor
                        </form>

                    </div>

                </div>

                <a id="pintar" class="waves-effect waves-light btn-large">
                    <i class="material-icons right">send</i>
                    Pintar
                </a>

                <div class="row">
                    <div id='chart_div' ></div>
                </div>


            </div>
            <div id="test3" class="col s12">

                <ul class="collection with-header">
                    @foreach($downloads as $download)
                        <a href="{{$url}}{{$directorio}}{{$download}}">
                            <li class="collection-item">
                                <div>
                                    {{$download}}
                                    <a href="{{$url}}{{$directorio}}{{$download}}" class="secondary-content">
                                        <i class="material-icons">get_app</i>
                                    </a>
                                </div>
                            </li>
                        </a>
                    @endforeach
                </ul>

            </div>
        </div>


    </div>
    <script>
        $("#pintar").on('click', function () {

            if($('select#chartType').val() == null){
                sweetAlert("AVISO", "Debes seleccionar un tipo de gráfica", "error");
            }else{
                pintarGrafica();
            }


        });
        $('select#chartType').change(function() {
           // console.log($(this).val())
        });


        $('#label input').on('change', function () {
            if ($("#value input[name=group2]").is(":checked")) {
                //alert("Los 2 check");
            } else {
                $("#value input:radio").attr('disabled', false);
            }
        });


    </script>
@endsection