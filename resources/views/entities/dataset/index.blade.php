@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col s12 titulo1 z-depth-1">
            <div class="container">
                <h3 class="center white-text">Listado de datasets</h3>
            </div>
        </div>
    </div>
    <div class="container">


        @if(count($datasets))
            <div class="collection">

                @foreach($datasets as $dataset)

                        <a class="collection-item" href="{{route('dataset.show',$dataset->id)}}" class="collection-item">{{$dataset->name}}<i class="material-icons secondary-content">assignment</i></a>

                @endforeach
            </div>
        @else
            <h3>No hay datasets aún.</h3>
        @endif
    </div>
@endsection