@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col s12 titulo1 z-depth-1">
            <div class="container">
                <h3 class="center white-text">Listado de categorías</h3>
                </div>
        </div>
        </div>
    <div class="container">


            {{--@foreach($categories as $category)--}}

                    {{--{{$category->name}}--}}
            {{--@endforeach--}}

        {{--@foreach($resultado as $categoria)--}}
            {{--<h3>{{$categoria['name']}}</h3>--}}
            {{--Subcategorías:--}}
            {{--@foreach($categoria['subcategories'] as $subcategoria)--}}
            {{--{{$subcategoria['name']}},--}}
            {{--@endforeach--}}
        {{--@endforeach--}}

        <div class="row">
        @foreach($resultado as $categoria)

                <div class="col s12 m6">
                    <div class="card blue darken-3 z-depth-2">
                        <div class="card-content white-text">
                            <span class="card-title"> <a href="{{url('dataset/cat',$categoria['id'])}}">{{$categoria['name']}}</a></span>
                            <p>{{$categoria['information']}}</p>
                        </div>
                        <div class="card-action">
                            <a class='dropdown-button btn' href='#' data-activates='{!! str_replace(' ', '_', $categoria['name']) !!}'>Subcategorías</a>
                            <ul id='{!! str_replace(' ', '_', $categoria['name']) !!}' class='dropdown-content'>
                                @foreach($categoria['subcategories'] as $subcategoria)
                                    <li><a href="{{url('dataset/subcat',$subcategoria['id'])}}">{{$subcategoria['name']}}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>



        @endforeach
    </div>

    </div>
@endsection