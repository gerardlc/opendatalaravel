@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <h3>Registro</h3>
        </div>
        <div class="row">
            <form class="form-horizontal col s12" role="form" method="POST" action="{{ url('/register') }}">
                {!! csrf_field() !!}

                <div class="row">
                    <div class="input-field col s12 {{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="name">Nombre</label>


                            <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}">

                            @if ($errors->has('name'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                            @endif

                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s12 {{ $errors->has('email') ? ' has-error' : '' }}">
                        <input id="email" type="email" class="validate" name="email" value="{{ old('email') }}">
                        <label for="email">Email</label>

                        @if ($errors->has('email'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s12 {{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password">Contraseña</label>

                            <input id="password" type="password" class="form-control" name="password">

                            @if ($errors->has('password'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif

                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s12 {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                        <label for="password">Confirmar contraseña</label>

                        <input id="password" type="password" class="form-control" name="password_confirmation">

                        @if ($errors->has('password_confirmation'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                        @endif

                    </div>
                </div>

                <div class="row">
                    <div class="form-group">
                            <button type="submit" class="waves-effect waves-light btn">
                                <i class="material-icons left">send</i> Enviar
                            </button>
                        </div>

                </div>


            </form>
        </div>
    </div>
    </div>
@endsection
