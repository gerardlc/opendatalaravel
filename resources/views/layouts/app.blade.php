<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Laravel</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    {!! MaterializeCSS::include_full() !!}

    <link rel="stylesheet" href="{{asset('css/main.css')}}">
    <link rel="stylesheet" href="{{asset('css/sweetalert.css')}}">
    <script src="{{asset('js/main.js')}}"></script>
    <script src="{{asset('js/sweetalert-dev.js')}}"></script>
    <link rel="icon" type="image/png" href="{{asset('img/logo2.png')}}" />


    <!--Load the AJAX API-->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>


</head>
<body id="app-layout">
            <nav class="blue-grey darken-1">
                <div class="nav-wrapper">
                    <div class="container">
                    <a href="{{ route('inicio')}}" class="brand-logo"><img src="{{asset('img/logo2.png')}}" alt=""></a>
                    <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
                    <ul class="right hide-on-med-and-down">
                        <li><a href="{{ route('inicio')}}">Inicio</a></li>
                        <li><a href="{{ route('category.index')}}">Categorias</a></li>
                        <li><a href="{{ route('subcategory.index')}}">Subcategorias</a></li>
                        <li><a href="{{ route('dataset.index')}}">Datasets</a></li>
                        @if (Auth::guest())
                            <li><a href="{{ url('/login') }}">Login</a></li>
                            <li><a href="{{ url('/register') }}">Register</a></li>
                        @else
                            <li>
                                <a href="#" >
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>
                            </li>


                            <li>
                                <a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i></a>
                            </li>


                        @endif
                    </ul>
                    <ul class="side-nav" id="mobile-demo">
                        <li><a href="{{ route('inicio')}}">Inicio</a></li>
                        <li><a href="{{ route('category.index')}}">Categorias</a></li>
                        <li><a href="{{ route('subcategory.index')}}">Subcategorias</a></li>
                        <li><a href="{{ route('dataset.index')}}">Datasets</a></li>
                        @if (Auth::guest())
                            <li><a href="{{ url('/login') }}">Login</a></li>
                            <li><a href="{{ url('/register') }}">Register</a></li>
                        @else
                            <li>
                                <a href="#">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a> </li>

                                    <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>


                        @endif
                    </ul>
                </div>
                </div>
            </nav>
@yield('content')





    <!-- JavaScripts -->
    {{----}}
    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>--}}
    {{----}}
    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>--}}
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}

</body>
</html>
