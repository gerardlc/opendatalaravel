<?php

namespace App\Http\Controllers;

use App\Category;
use App\Dataset;
use App\Subcategory;
use Illuminate\Auth\Access\Response;
use Illuminate\Http\Request;
//use Illuminate\Support\Facades\Request;

//para poder usar el request ajax

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;//para poder usar input post ajax

class DatasetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datasets = Dataset::all();
        return view('entities.dataset.index',["datasets"=>$datasets]);
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('entities.dataset.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $dataset = new Dataset();
        $dataset->name = $request->nombre;
        $dataset->information = $request->informacion;
        $dataset->table = $request->tabla;
        $dataset->downloads = $request->descargas;
        $dataset->category_id = $request->categoria;
        $dataset->subcategory_id = $request->subcategoria;

        $dataset->save();
        return redirect('dataset');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

//        if (Request::ajax())
//        {
//            $data[] = array('label'=> 'Raval2','Poblacion'=> 9);
//            $data[] = array('label'=>'proba','id'=>session('key'));
//            return $data;
       // }
        $dataset = Dataset::find($id);
        $downloads = $porciones = explode(",", $dataset->downloads);
        session(['tabla' => $dataset->table]);
        //$result = DB::table($dataset->table)->pluck('numero_personas');
       // $result2 = DB::table($dataset->table)->select('numero_personas', 'barrioOrigen')->groupBy('barrioOrigen')->get();
        $data = array('Mushrooms'=> 3,
            'Onions'=> 1
        );
       // var_dump($data);

        $columns = DB::getSchemaBuilder()->getColumnListing($dataset->table);
        //print_r($columns);
        $rows = DB::table($dataset->table)->paginate(15);

        return view('entities.dataset.show',["rows"=>$rows,'columns'=>$columns, 'data'=>$data, "url" => config('variables.base_url'), "downloads"=>$downloads, "directorio" => "uploads/".$dataset->table."/"]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showByCat($id){
        $datasets = Category::find($id)->datasets;

        return view('entities.dataset.index',["datasets"=>$datasets]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showBySubcat($id){
        $datasets = Subcategory::find($id)->datasets;

        return view('entities.dataset.index',["datasets"=>$datasets]);
    }

    
//    public function datos()
//    {
//
//        $data[] = array('label'=> 'Raval','Poblacion'=> 5);
//        $data[] = array('label'=> 'Raval2','Poblacion'=> session('tabla'));
//        return $data;
//    }



    public function datos2()
    {
//        if (Request::ajax())
//        {
            $label = Input::get('label');
            $value = Input::get('value');


            $result2 = DB::table(session('tabla'))->select(DB::raw('CONVERT(SUM('.$value.'), SIGNED INTEGER) as '.$value.', '.$label))->groupBy($label)->get();


           // $result2 = DB::table(session('tabla'))->select($label, SUM($value))->groupBy($label)->get();

            $data[] = array('label'=>'proba','id'=>5);
            return $result2;
       // }


        $data[] = array('label'=> 'Raval2','Poblacion'=> 5);
        return $data;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
