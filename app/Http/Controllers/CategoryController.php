<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Category;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        $resultado = array();

        //cada categoria
        foreach ($categories as $category){
            $subcategoriesaux=Category::find($category['id'])->subcategories;
            $subresult=array();

            //cada subcategoria de la categoria
            foreach ($subcategoriesaux as $subcategory){
                $subresult[]=array('name'=>$subcategory['name'],'id'=>$subcategory['id']);
            }

            $resultado[]= array(
                'id'=>$category['id'],
                'name'=>$category['name'],
                'information'=>$category['information'],
                'subcategories'=>$subresult
                );

        }

        //print_r($resultado);
        return view('entities.category.index', ['resultado'=>$resultado]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('entities.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $category = new Category();
        $category->name = $request->nombre;
        $category->information = $request->informacion;
        $category->image = $request->imagen;

        $category->save();
        return redirect('category');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
