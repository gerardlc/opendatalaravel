<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('inicio');

Route::auth();

//Route::get('dataset/datos', 'DatasetController@datos');
Route::post('dataset/datos2', 'DatasetController@datos2');
Route::get('dataset/cat/{id}', 'DatasetController@showByCat');
Route::get('dataset/subcat/{id}', 'DatasetController@showBySubcat');


Route::get('/home', 'HomeController@index');
Route::resource('category', 'CategoryController');
Route::resource('subcategory', 'SubcategoryController');
Route::resource('dataset', 'DatasetController');



Route::get('/categories', function() {
    return "Hello there!";
});