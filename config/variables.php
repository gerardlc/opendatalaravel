<?php
/**
 * Created by PhpStorm.
 * User: Gerard
 * Date: 25/05/2016
 * Time: 22:47
 */
return [

    /*
    |--------------------------------------------------------------------------
    | Default Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default filesystem disk that should be used
    | by the framework. A "local" driver, as well as a variety of cloud
    | based drivers are available for your choosing. Just store away!
    |
    | Supported: "local", "ftp", "s3", "rackspace"
    |
    */

    'base_url' => 'http://localhost/opendatacollector/public/'
];