/**
 * Created by Gerard on 10/05/2016.
 */
$( document ).ready(function(){


    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#get').click(function(){
        $.ajax({
            url: "datos",
            type:"get",
            beforeSend: function(xhr){
                xhr.setRequestHeader('X-CSRF-TOKEN', $("#token").attr('content'));
            },
            success:function(data){
                console.log(data);
            },error:function(){
                alert("error!!!!");
            }
        });

    });


    $(".button-collapse").sideNav();
    $('.parallax').parallax();


    $('.dropdown-button').dropdown({
            inDuration: 300,
            outDuration: 225,
            constrain_width: true, // Does not change width of dropdown to that of the activator
            hover: true, // Activate on hover
            gutter: 0, // Spacing from edge
            belowOrigin: false, // Displays dropdown below the button
            alignment: 'left' // Displays dropdown with edge aligned to the left of button
        }
    );

    $('select').material_select();
});